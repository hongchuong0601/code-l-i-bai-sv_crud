// tạo DS rỗng
var dssv = [];

// lấy dữ liệu từ localStorage lúc user load trang
var dataJson = localStorage.getItem("DSSV");
if (dataJson != null){
    dssv = JSON.parse(dataJson).map(function(item){
        return new SinhVien(item.ma, item.ten, item.email, item.pass, item.toan, item.ly, item.hoa,)
    });
    renderDSSV(dssv);
}



function themSinhVien(){
    // tạo sv
    var sv = layThongTinTuForm();
    dssv.push(sv);
    
    
     
    //render dssv
    renderDSSV(dssv);

    // save dssv vào localStorage
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson);
    // reset => dùng thẻ form để reset thẻ input
    document.getElementById("formQLSV").reset ();
}

function xoaSinhVien(id){
    
    var index = dssv.findIndex(function (item){
        return item.ma == id
    });
    // splice = cut
    // slice = copy
    dssv.splice(index, 2)
    renderDSSV(dssv);

    //muốn cập nhật lại local cần thêm
    // var dataJson = JSON.stringify(dssv);
    // localStorage.setItem("DSSV", dataJson);
}

function suaSinhVien(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id
    });
    
    // show thông tin lên form
    showThongTinLenForm(dssv[index]);
}

